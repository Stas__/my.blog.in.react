
import React from "react";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import { BrowserRouter as Router,Switch,Route,Link,Redirect,useLocation, } from "react-router-dom";
import MYWORK from "./MYWORK";
import SKILLS from "./SKILLS";
import ABOUTMYSELF from "./ABOUTMYSELF";
import '../App.css';



 function Mainblock() {
  return (
    <Router>
      <Switch>
        <Route exact path="/home">
          <Redirect to="/" />
        </Route>
        <Route>
          <Saite />
        </Route>
      </Switch>
    </Router>
  );
}


export default Mainblock



function Saite() {
  let location = useLocation();

  return (
    <div style={styles.fill}>
      <ul className="mainmenu" style={styles.nav}>
        <NavLink to="/mywork">MYWORK</NavLink>
        <NavLink to="/skills">SKILLS</NavLink>
        <NavLink to="/about-myself">ABOUT MYSELF</NavLink>
       
      </ul>

      <div style={styles.content}>
        <TransitionGroup>
          {}
          <CSSTransition
            key={location.key}
            className="time"
            timeout={350}
          >
            <Switch location={location}>
                  <div className="backgroundblock">
                      <Route path="/mywork" children={<MYWORK />} />
                      <Route path="/skills" children={<SKILLS />} />
                      <Route path="/about-myself" children={<ABOUTMYSELF />} />
                  </div>
            </Switch>
          </CSSTransition>
        </TransitionGroup>
      </div>
    </div>
  );
}

function NavLink(props) {
  return (
    <li style={styles.navItem}>
      <Link {...props} />
    </li>
  );
}

const styles = {};

styles.fill = {
  position: "absolute",
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  zindex:-21
  
};


