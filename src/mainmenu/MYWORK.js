import React from 'react';

import '../App.css';
import { makeStyles } from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import Typography from '@material-ui/core/Typography';

   const imageblock = [ 
    {
      width: '30%',
      art1: 'ARTALLO',
      
    },
    { 
     
    width: '30%',
    art2: 'INTERNET-PORATAL',
    
    },
  {
    width: '30%',
    art3: 'WAVEATTACHMENT',
   
    },
  { 
    width: '30%',
    art4: 'SPORT-FOOD',
  },
  { 
    width: '30%',
    art5: 'SAITMASUSHI',
  
  },
  { 
    width: '30%',
    art6: 'DESLAYOUTSCREEN',
  },
  { 
    width: '30%',
    art7: 'DISCAVTONEWS',
    
  },
  { 
    width: '30%',
    art8: 'STMIRONENKO',
  },
  { 
    width: '30%',
    art9: 'STASMIRONENKO',
    
  },
  
];





  
 



const useStyles = makeStyles((theme) => ({
  
  root: {
    display:'flex',
    flexDirection:'row',
    flexWrap:'wrap',
    maxWidth:'100%',
    justifyContent:'center',
    gap: '2rem',
    
    
  },
  
  image: {
    height: '31%',
    alignItems:'flex-start',
    fontSize:"2vw",
    flexWrap:'wrap',
    marginTop:'3vw',
    marginBottom:'3vw',
    zIndex:21,
    [theme.breakpoints.down('xs')]: {
      width: '100%', 
      
    },
    '&:hover, &$focusVisible': {
      zIndex: 1,
      color:"blue",
      transition:'1.4s',

      '& $imageBackdrop': {
        opacity: 0.36,
        background:'green',
        
      },
      
      
    },
    
  },
  
  imageButton: {
    
    color: theme.palette.common.grey,
    
  },
  
  imageBackdrop: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom:0,
    backgroundColor: theme.palette.common.black,
    opacity: 0.4,
    transition: theme.transitions.create('opacity'),
    transition:'1.2s',
   
        
  },
  
 
}));

const link1 = {
  color:'black',
  zIndex:1,
  position: 'absolute',
  top: '37%',
  left:0,
  right:0,
  wordWrap: 'break-word',
  wordBreak:'break-all',
  
}
const link2 = {
  color:'black',
  zIndex:1,
  position: 'absolute',
  top: '37%',
  left:0,
  right:0,
  wordWrap: 'break-word',
  wordBreak:'break-all'
}
const link3 = {
  color:'black',
  zIndex:1,
  position: 'absolute',
  top: '37%',
  left:0,
  right:0,
  wordWrap: 'break-word',
  wordBreak:'break-all'
}
const link4 = {
  color:'black',
  zIndex:1,
  position: 'absolute',
  top: '37%',
  left:0,
  right:0,
  wordWrap: 'break-word',
  wordBreak:'break-all'
  
}
const link5 = {
  color:'black',
  zIndex:1,
  position: 'absolute',
  top: '37%',
  left:0,
  right:0,
  wordWrap: 'break-word',
  wordBreak:'break-all',
 
}
const link6 = {
  color:'black',
  zIndex:1,
  position: 'relative',
  top: '-0.5em',
  left:0,
  right:0,
  wordWrap: 'break-word',
  wordBreak:'break-all'
}
const link7 = {
  color:'black',
  zIndex:1,
  position: 'relative',
  top: '1em',
  left:0,
  right:0,
  wordWrap: 'break-word',
  wordBreak:'break-all'
}
const link8 = {
  color:'black',
  position: 'relative',
  bottom: '1.2em',
  left:0,
  right:0,
  zIndex:1,
  wordWrap: 'break-word',
  wordBreak:'break-all'

}
const link9 = {
  color:'black',
  position: 'relative',
  bottom: '3.2em',
  left:0,
  right:0,
  zIndex:1,
  wordWrap: 'break-word',
  whiteSpasing:'normal',
  wordBreak:'break-all'
}



 function MYWORK() {
  const classes = useStyles();
  
  

    return (
     
      <div className={classes.root}>
                       
              {imageblock.map((image) => (
                
                <ButtonBase
                  focusRipple
                    
                  className={classes.image}
                  
                  focusVisibleClassName={classes.focusVisible}
                  style={{width: image.width}}
                > 
                <div className={classes.imageName}>
                      <div classNam={classes.imageNam}>
                          <h3><a href="https://artalloproduct.github.io/index.html" style={link1}>{image.art1}</a></h3>
                          <h3><a href="https://internet-portal.000webhostapp.com/options.html"  style={link2}>{image.art2}</a></h3>
                          <h3><a href="https://waveattachment.000webhostapp.com/serfopen.html"  style={link3}>{image.art3}</a></h3>
                          <h3><a href="https://sport-food.github.io/article.html"  style={link4}>{image.art4}</a></h3>
                          <h3><a href="https://saitamasushi.github.io/online.html"  style={link5}>{image.art5}</a></h3>
                          <h3><a href="https://deslayoutscreen.github.io"  style={link6}><p>{image.art6}</p></a></h3>
                          <h3><a href="http://discavtonews.zzz.com.ua/directory.html"  style={link7}><p>{image.art7}</p></a></h3>
                          <h3><a href="https://stmironenko.github.io"  style={link8}><p>{image.art8}</p></a></h3>
                          <h3><a href="https://stasmironenko.github.io"  style={link9}><p>{image.art9}</p></a></h3>                     
                      
                      
                                  <span className={classes.imageBackdrop}  />
                                  <span className={classes.imageButton}>
                                     <Typography component="span" color="inherit" 
                                className={classes.imageTitle}>
                            </Typography>
                                  </span>
                      </div>
                </div>
                </ButtonBase>
              ))}
       
      </div>
      
     


  );

 
}

export default MYWORK 


