import React from "react";
import "./App.css";

import Mainblock from "./mainmenu/Mainblock";
import Contentblock from "./contentmenu/Contentblock";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


function App() {
  return (
      <Router>
                   <Route path="/" component={Mainblock} />
          
        <div className="App">
        <Switch>
            <div className="block">
                   <Route path="/" component={Contentblock} />
                   
            </div>
        </Switch>
      </div>
    </Router>
  );
}

export default App;



