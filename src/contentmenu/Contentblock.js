import React from 'react';
import '../App.css';
import Home from "./Home";
import Contact from "./Contact";
import About from "./About";
import Mainpage from "./About";
import Gallery from "./Gallery";

import New from "./New";


import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';


function Contentblock() {
  return (
           
           
           <Router>
                      <New />
                   <div className="App"> 
                      <Switch>
                              <Route path="/home" component={Home} />
                              <Route path="/gallery" component={Gallery} />
                              <Route path="/contact" component={Contact} />
                              <Route path="/about" component={About} />
                        
                      </Switch>
                   </div>
                            
                                
                  
           </Router>
          

  );
   
} 
    export default Contentblock
