import React from "react";
import "../App.css";
import Newcontact from "../Newcontact";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function Contact() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <div className="block">
                <Route path="/" component={Newcontact} />
          </div>
        </Switch>
      </div>
    </Router>
  );
}

export default Contact;
