import React from "react";
import "../App.css";
import NewHome from "./NewHome";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function Home() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <div className="block">
                 <Route path="/" component={NewHome} />
          </div>
        </Switch>
      </div>
    </Router>
  );
}

export default Home;
